node {
    checkout scm

    def registry = 'docker.flw.ovh'
    def registryCredential = 'docker.flw.ovh'
    def dockerImage = 'k8s/k8micro'


    String jdktool = tool name: "java8", type: 'hudson.model.JDK'
    def mvnHome = tool name: 'maven3'

    /* Set JAVA_HOME, and special PATH variables. */
    List javaEnv = [
        "PATH+MVN=${jdktool}/bin:${mvnHome}/bin",
        "M2_HOME=${mvnHome}",
        "JAVA_HOME=${jdktool}"
    ]

    withEnv(javaEnv) {
        stage ('Initialize') {
            sh '''
                echo "PATH = ${PATH}"
                echo "M2_HOME = ${M2_HOME}"
            '''
        }
    }




    stage ('Build Maven') {
        try {

            /* micronauts da problemi se non è stato fatto un clean per sicurezza lo faccio anche prima della build */

            sh 'mvn clean -B -T4C'

            sh 'mvn package -Dmaven.test.skip=true -Dmaven.javadoc.skip=true -Dsource.skip=true dependency:copy-dependencies -B -T4C'
        } catch (e) {
            currentBuild.result = 'FAILURE'
        }
    }


    def customImage = null
    def customImageName = null

    stage ('Build Docker') {

        def shortCommit = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()

        echo 'Commit...' + shortCommit

        customImage = docker.build(dockerImage + ":" + shortCommit)

        customImageName = registry + '/' + dockerImage + ":" + shortCommit
    }



    stage ('Push Docker'){

        docker.withRegistry('https://' + registry, registryCredential ) {
            customImage.push()

            customImage.push('latest')
        }

    }

    stage ('Kubectl deploy'){

        if (customImageName != null)
        {
            sh 'kubectl apply -f metadata/k8micro.yaml'
            sh 'kubectl set image deployment/k8micro-deployment k8micro=' + customImageName
        }
    }


    stage("Clean all") {
        sh 'mvn clean -B -T4C'
    }


}