package k8micro;

import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Post;

import javax.annotation.Nullable;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Controller("/")
public class ProvaController {

    @Get
    public HttpStatus index() {
        return HttpStatus.OK;
    }
//
//    @Get("/health")
//    public String health () {
//        return DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss").format(LocalDateTime.now());
//    }

    @Get("/hello")
    public String hello(@Nullable String name) {
        if (name == null || name.isEmpty()) {
            name = "world";
        }
        return name;
    }

    @Get("/pippo")
    public String pippo(@Nullable String name) {
        if (name == null || name.isEmpty()) {
            name = "plutone";
        }
        return name;
    }


}