package k8micro;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.protobuf.Descriptors;
import com.netflix.conductor.common.metadata.tasks.Task;
import com.netflix.conductor.common.metadata.tasks.TaskResult;
import com.netflix.conductor.common.run.Workflow;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;

import javax.inject.Inject;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;


@Controller("/conductor")
public class ConductorController {
//    private static String METADATA_PATH = "/api/metadata";
//    private static String TASK_METADATA_PATH = METADATA_PATH + "/taskdefs";
//    private static String WORKFLOW_METADATA_PATH = METADATA_PATH + "/workflow";
    private static String WORKFLOW_PATH = "/api/workflow/";
    private static String UPDATE_TASK_PATH = "/api/tasks";


    //@Client("conductor-server")
    @Client("http://192.168.254.18:8080")
    @Inject
    private RxHttpClient conductorServer;


    @Inject
    private KafkaProducer producer;

    @Post("/start")
    public HttpResponse<String> start(HttpHeaders headers, @Body Map<String,Object> body) {

        String workflow = headers.get("workflow");
        String wait_task = headers.get("wait_task");


        Map<String,Object> results = new HashMap<String, Object>(){{
            put("ok","ok");
        }};

        producer.sendRequest("Nike", "ok");

        end_task_delay(workflow, wait_task, results);


//        return HttpResponse.created(rq);
        return HttpResponse.ok();
    }



    @Post("/actionRandom")
    HttpResponse<String> actionRandom(HttpHeaders headers,@Body Map<String,Object> body){

        String workflow = headers.get("workflow");
        String wait_task = headers.get("wait_task");

        String start_element = (String) body.get("value");

        int value = -1;

        if(start_element.equals("A")) {
            value=0;
        }
        if(start_element.equals("B")) {
            value=1;
        }

        int finalValue = value;
        Map<String,Object> results = new HashMap<String, Object>(){{
            put("value", finalValue);
        }};

        end_task_delay(workflow, wait_task, results);

        return HttpResponse.ok("" + finalValue);
    }


    @Post("/actionOdd")
    HttpResponse<String> actionOdd(HttpHeaders headers,@Body Map<String,Object> body) throws Exception {
        String workflow = headers.get("workflow");
        String wait_task = headers.get("wait_task");

        int value = Integer.parseInt(""+ body.get("value"));

        if(value % 2 !=0)
            throw new Exception("value not odd");

        Map<String,Object> results = new HashMap<String, Object>(){{
            put("value", "Pari");
        }};

        end_task_delay(workflow, wait_task, results);

        return HttpResponse.ok("Pari");
    }



    @Post("/actionEven")
    HttpResponse<String> actionEven(HttpHeaders headers,@Body Map<String,Object> body) throws Exception {
        String workflow = headers.get("workflow");
        String wait_task = headers.get("wait_task");

        int value = Integer.parseInt(""+ body.get("value"));

        if(value % 2 ==0)
            throw new Exception("value not even");

        Map<String,Object> results = new HashMap<String, Object>(){{
            put("value", "Dispari");
        }};

        end_task_delay(workflow, wait_task, results);

        return HttpResponse.ok("Dispari");
    }

    @Post("/end")
    public HttpResponse<String> end(@Body  Map<String,Object> body) throws IOException {

        String workflow = (String) body.get("workflowId");
        String wait_task = (String) body.get("taskRefName");


        end_task(workflow,wait_task, (Map<String, Object>) body.get("results"), TaskResult.Status.COMPLETED);

        return HttpResponse.ok("ok");
    }

//    private void end_task(String workflowId, String waitTaskRefName, Map<String,Object> results){
//
//        Map<String,Object> body_request = new HashMap<String, Object>(){{
//
//            put("workflowId", workflowId);
//            put("taskRefName", waitTaskRefName);
//            put("results", results);
//
//        }};
//
//
//        HttpRequest end_request = HttpRequest.POST("/complete",body_request);
//        conductorCoordinator.exchange(end_request, String.class).blockingFirst();
//    }


    public class ConductoClientClassKeyDeserializer extends KeyDeserializer {
        @Override
        public Object deserializeKey(final String key, final DeserializationContext ctxt) throws IOException, JsonProcessingException {
            return null; // replace null with your logic
        }
    }

    private void end_task(String workflowId, String waitTaskRefName, Map<String,Object> results, TaskResult.Status taskStatus) throws IOException {

        HttpRequest get_worflow = HttpRequest.GET(WORKFLOW_PATH + workflowId);

        ObjectMapper om;

        om = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addKeyDeserializer(Descriptors.FieldDescriptor.class, new ConductoClientClassKeyDeserializer());
        om = om.registerModule(simpleModule).configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        String wf_string = (String) conductorServer.retrieve(get_worflow, String.class).blockingFirst();

        Workflow wf = om.readValue(wf_string, Workflow.class);

        Task task = wf.getTaskByRefName(waitTaskRefName);

        if (task.getStatus().equals(Task.Status.IN_PROGRESS)) {

            TaskResult taskResult = new TaskResult(task);

            taskResult.setStatus(taskStatus);
            taskResult.setOutputData(results);

            HttpRequest update_wait = HttpRequest.POST(UPDATE_TASK_PATH, taskResult);
            conductorServer.exchange(update_wait, String.class).blockingFirst();
        }


    }

    private void end_task_delay(String workflow, String wait_task, Map<String, Object> results) {
        Timer t = new Timer();
        t.schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {


                        try {
                            end_task(workflow,wait_task,results, TaskResult.Status.COMPLETED);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        // your code here
                        // close the thread
                        t.cancel();
                    }
                },
                5000
        );
    }

}
