package k8micro;

import io.micronaut.configuration.kafka.annotation.*;

@KafkaListener(offsetReset = OffsetReset.EARLIEST, groupId="myGroup")
public class KafkaConsumer {

    @Topic("${KAFKA_TOPIC:k8micro}")
    public void getRequest(@KafkaKey String uID, String value) {
        System.out.println("Got Product - " + uID + " by " + value);
    }
}