package k8micro;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Controller("/health")
public class HealthController {

    @Get
    public String index () {


        //ADD HERE OTHER CHECK
        return DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss").format(LocalDateTime.now());
   }
}
