package k8micro;


import io.micronaut.configuration.kafka.annotation.KafkaClient;
import io.micronaut.configuration.kafka.annotation.KafkaKey;
import io.micronaut.configuration.kafka.annotation.Topic;

@KafkaClient
public interface KafkaProducer {

    @Topic("${KAFKA_TOPIC:k8micro}")
    void sendRequest(@KafkaKey String uID, String value);
}