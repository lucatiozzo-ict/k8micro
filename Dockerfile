FROM openjdk:8u171-alpine3.7
RUN apk --no-cache add curl
COPY target/k8micro*.jar k8micro.jar
CMD java ${JAVA_OPTS} -jar k8micro.jar